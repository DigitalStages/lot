
## Examples >> LoT_Logo

### Title
StarterKit >> Examples >> LoT_Logo
This is an part of StarterKit >> Examples.
<br>

### Author
Jens Meisner
<br>

### Date
24/11/2020
<br>

### Images
<br>

| Image 1      | Image 2    |
| :------------- | ---------- |
| <img src="images/screenshot_256.jpg" width="256"/> | <img src="images/ph_lot_1024.jpg" width="256"/> |
| Image 3      | Image 4    |
| <img src="images/ph_lot_1024.jpg" width="256"/> | <img src="images/ph_lot_1024.jpg" width="256"/> |

<br>

### 3d Preview
The design can be viewed in 3D by clicking on the file with .stl extension under /files
<br>

### Description
This is a little logo button with extruded text to be printed via 3D printer
<br>

### BOM
No extra parts
<br>

### Notes
It is not tested as print, it only is used for visualization.
<br>
