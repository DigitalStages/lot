/*File Info--------------------------------------------------------------------
File Name: LoT_Logo.scad
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 16/01/20
Desc:  This is an example .scad file for a template
Usage: Use this template to upload your design to LoT
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------

//If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below
//include<lot_connectors.scad>
//include<lot_utilities.scad>


//Logo Design

//Parametric values
diameter=100;
height=20;
fn=80;

//Cylindrical button with carved in pocket part
difference()
{
    cylinder(d=diameter,h=height,center=true,$fn=fn);
    translate([0,0,height/2])
    cylinder(d=diameter*9/10,h=height/2,center=true,$fn=fn);
}
//Extruded 2D Text with different sizes and fonts
linear_extrude(20,center=true){
    union()
    {
        translate([0,diameter/14,0])
        text("LoT",size=diameter/4,valign="center",halign="center",font = "Bitstream:style=Bold");
        translate([0,-diameter/8,0])
        text("Library of Things",size=diameter/16,valign="center",halign="center",font = "FreeSans:style=Bold");
        translate([0,-diameter/4,0])
        text("cc-by-sa",size=diameter/20,valign="center",halign="center",font = "FreeSans:style=Bold");
    }
}
