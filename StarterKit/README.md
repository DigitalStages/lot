![](Starterkit.png)
</br>
## StarterKit

The StarterKit contains files, template and examples, that helps to keep an certain structure within the Library of Things and simplify the modeling in OpenSCAD. This is beneficial to all participants to keep a certain name convention and file/folder structure.

#### **Please follow the guide of how to use the template to add designs and files to the LoT explained in HowTo >> LoT**

### Includes

* **Modules** >> This are the basic modules of connectors, and support elements to simplify the creation of designs
* **Template** >> This is a basis template to be used for every new design, that makers from the community want to add to the Library
* **Examples** >> Contains a few example to see how a finished upload ready project looks like
