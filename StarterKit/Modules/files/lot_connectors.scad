/* File Info--------------------------------------------------------------------------------------------------------------------------
File Name: lot_connectors.scad
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: DigitalStages - Jens Meisner
Date: 08/01/20
Desc:  This file is part of the Library of Things, 
    and contains connector modules for CNC and 3D Print designs, 
    e.g. furniture, household items.
Usage: Use "include <lot_connectors.scad>" or "use <lot_utilities.scad>" to import 
    this module library. 
/*
/* Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
// Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


// Uncomment following default variables to make examples below work properly
toolsize = 2;
thickness = 8;
x = thickness * 3;
y = thickness;
z = thickness;
y1 = thickness * 2;
y2 = thickness * 3;
roughness = 0.01;
edged = 0;
roundness = 8;


// CONNECTORS FOR CNC DESIGNS-----------------------------------------------------------------------------------------------------------

// Connection plug "DogBone"
module dogbone_plug(x, y, z, toolsize, roughness, edged = 0, resolution = 20)
{
    difference()
    {
        union()
        {
            
            translate([0, y / 2, z / 2])
            cube([x + (toolsize * 4), y, z], center = true);
            translate([0, -y / 2, z / 2])
            cube([x - roughness, y, z], center = true);
        }
        union()
        {
            if(edged == 0 || edged == -1)
            {
                translate([x / 2 + toolsize / 3 - roughness / 2, -toolsize / 3, z / 2]) 
                cylinder(h = z, d = toolsize, center = true, $fn = resolution);
                if(edged == -1)
                {
                    translate([-x / 2 - toolsize, y / 2, z / 2])
                    cube([toolsize * 2, y, z], center = true);
                }
            }
            if(edged == 0 || edged == 1)
            {
                translate([-x / 2 - toolsize / 3 + roughness / 2, -toolsize / 3, z / 2]) 
                cylinder(h = z, d = toolsize, center = true, $fn = resolution);
                if(edged == 1)
                {
                    translate([x / 2 + toolsize, y / 2, z / 2])
                    cube([toolsize * 2, y, z], center = true);
                }
            }
        }
    }
}

//Connection slot "DogBone"
module dogbone_slot(x, y, z, toolsize, roughness, edged = 0, resolution = 20)
{
    union()
    {
        translate([0, 0, z / 2])
        cube([x+roughness, y + roughness, z], center = true);
         
        if(edged == 0 || edged == -1)
        {
            translate([x / 2 - toolsize / 3 + roughness / 2, y / 2 - toolsize / 3 + roughness / 2, z / 2])
            cylinder(h=z, d = toolsize, center = true, $fn = resolution);
            translate([x / 2 - toolsize / 3 + roughness / 2, -y / 2 + toolsize / 3 - roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
        }
        if(edged == 0 || edged == 1)
        {
            translate([-x / 2 + toolsize / 3 - roughness / 2, y / 2 - toolsize / 3 + roughness / 2, z / 2])
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([-x / 2 + toolsize / 3 - roughness / 2, -y / 2 + toolsize / 3 - roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
        } 
    }
}


// Connection plug "TBone"
module tbone_plug(x, y, z, toolsize, roughness, edged = 0, resolution = 20)
{
    difference()
    {
        union()
        {
            translate([0, y / 2, z / 2])
            cube([x + (toolsize * 4), y, z], center = true);
            translate([0, -y / 2, z / 2])
            cube([x - roughness, y, z], center = true);
        }
        if(edged == 0 || edged == -1)
        {
            translate([x / 2 + toolsize / 2 - roughness / 2, 0, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            if(edged == -1)
            {
                translate([-x / 2 - toolsize, y / 2, z / 2])
                cube([toolsize * 2, y, z], center = true);
            }
        }
        if(edged == 0 || edged == 1)
        {
            translate([-x / 2 - toolsize / 2 + roughness / 2, 0, z / 2])
            cylinder(h = z, d = toolsize, center = true, $fn = resolution); 
            if(edged == 1)
            {
                translate([x / 2 + toolsize, y / 2, z / 2])
                cube([toolsize * 2, y, z], center = true);
            }
        }
    }
}

//Connection slot "TBone"
module tbone_slot(x, y, z, toolsize, roughness, edged = 0, resolution = 20)
{
    union()
    {
        translate([0, 0, z / 2])
        cube([x + roughness, y + roughness, z], center = true);
        if(edged == 0 || edged == -1)
        {
            translate([x / 2 - toolsize / 2 + roughness / 2, y / 2 + roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([x / 2 - toolsize / 2 + roughness / 2, -y / 2 - roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
        }
        if(edged == 0 || edged == 1)
        {
            translate([-x / 2 + toolsize / 2 - roughness / 2, y / 2 + roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution); 
            translate([-x / 2 + toolsize / 2 - roughness / 2, -y / 2 - roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
        }  
    }
}


//Connection slot "Sniglet"
module sniglet_slot(x, y, z, toolsize, roughness, edged = 0, resolution = 20)
{
    difference()
    {
        union()
        {
            translate([0, 0, z / 2])
            cube([x + roughness, y + roughness, z], center = true);
            if(edged == 0 || edged == -1)
            {
                translate([x / 2 - toolsize / 2 + roughness / 2, y / 2 + toolsize / 2 + roughness / 2, z / 2]) 
                cylinder(h = z, d = toolsize, center = true, $fn = resolution);
                translate([x / 2 - toolsize / 2 + roughness / 2, -y / 2 - toolsize / 2 - roughness / 2, z / 2]) 
                cylinder(h = z, d = toolsize, center = true, $fn = resolution);
                translate([x / 2 - toolsize / 2 - toolsize / 4 + roughness / 2, 0, z / 2])
                cube([toolsize * 1.5, y + toolsize, z], center = true);
            }
            if(edged == 0 || edged == 1)
            {
                translate([-x / 2 + toolsize / 2 - roughness / 2, y / 2 + toolsize / 2 + roughness / 2, z / 2]) 
                cylinder(h = z, d = toolsize, center = true, $fn = resolution);
                translate([-x / 2 + toolsize / 2 - roughness / 2, -y / 2 - toolsize / 2 - roughness / 2, z / 2]) 
                cylinder(h = z, d = toolsize, center = true, $fn = resolution);  
                translate([-x / 2 + toolsize / 2 + toolsize / 4 - roughness / 2, 0, z / 2])
                cube([toolsize * 1.5, y + toolsize, z], center = true);
            }
            
            
        }
        union()
        {
            translate([x / 2 - toolsize * 1.5 + roughness / 2, y / 2 + toolsize / 2 + roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([-x / 2 + toolsize * 1.5 - roughness / 2, y / 2 + toolsize / 2 + roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([x / 2 - toolsize * 1.5 + roughness / 2, -y / 2 - toolsize / 2 - roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([-x / 2 + toolsize * 1.5 - roughness / 2, -y / 2 - toolsize / 2 - roughness / 2, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);  
        }
    }
}


// Connection plug "T Wedge" 
// Be aware, that the wedge will be smaller as slot is giving fillet_x and fillet_y
module twedge_plug(x, y, z, toolsize, roughness, cap = 0, resolution = 20)
{
    difference()
    {
        union()
        {
            //Basic body 
            translate([0, y / 2, z / 2])
            cube([x - roughness - (z * 2) + toolsize * 6, y, z], center = true);
            translate([0, -y / 2, z / 2])
            hull()
            {
                cube([x - roughness - z * 2, y * 1.5, z], center = true);
                translate([0, -y * 2, 0])
                cylinder(d = x * 0.3, h = z, center=true, $fn = resolution);
            }
        }
        
        union()
        {
            //CNC drill hole extensions 
            translate([-x / 2 - toolsize / 2 + z, 0, z / 2])
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([x / 2 - z + toolsize / 2 + roughness / 2, 0, z / 2])
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
        }
    }
    if(cap == 1)
    {
        //Add round cap
        difference()
        {
            scale([1, 0.5, 1])
            translate([0, y * 2, thickness / 2])
            cylinder(d = x - roughness - z * 2 + toolsize * 6, h = z, center=true);
            translate([0, y / 2, z / 2])
            cube([x - roughness - z * 2 + toolsize * 6, y, z], center = true);
        }
    }
}

// Connection slot "T Wedge"
module twedge_slot(x, y, z, toolsize, roughness, resolution = 20)
{
    difference()
    {
        union()
        {
            //Basic body
            translate([0, y / 2, z / 2])
            cube([x + (toolsize * 4), y, z], center = true);
            translate([0, -y / 2, z / 2])
            hull()
            {
                cube([x - roughness, y * 1.5, z], center = true);
                translate([x / 2 - z / 2, -y * 2, 0])
                cylinder(d = z, h = z, center=true, $fn = resolution);
                translate([-x / 2 + z / 2, -y * 2, 0])
                cylinder(d = z, h = z, center=true, $fn = resolution);
            }
        }
        union()
        {
            //CNC drill hole extensions
            translate([x / 2 + toolsize / 2 - roughness / 2, 0, z / 2]) 
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([-x / 2 - toolsize / 2 + roughness / 2, 0, z / 2])
            cylinder(h = z, d = toolsize, center = true, $fn = resolution);
            translate([0, -y * 1.5, 0])
            dogbone_slot(x - z * 2, z, z, toolsize, roughness);
        }
    }
}


// Connection plug "Dovetail"
module dovetail_plug(x, y1, y2, z, roundness, roughness, resolution = 20)
{
    union()
    {
        hull()
        {
            translate([z / 2, 0, z / 2])
            cube([z - roughness + 1, y1 - roughness, z], center=true);
            translate([x / 2 - roundness / 2, y2 / 2 - roundness / 2, 0])
            cylinder(d=roundness, h=z, $fn=resolution);
            translate([x / 2 - roundness / 2, -y2 / 2 + roundness / 2, 0])
            cylinder(d=roundness - roughness / 2, h=z, $fn=resolution);
        }
        mirror([1, 0, 0])
        hull()
        {
            translate([z / 2, 0, z / 2])
            cube([z - roughness + 1, y1 - roughness, z], center=true);
            translate([x / 2 - roundness / 2, y2 / 2 - roundness / 2, 0])
            cylinder(d=roundness, h=z, $fn=resolution);
            translate([x / 2 - roundness / 2, -y2 / 2 + roundness / 2, 0])
            cylinder(d=roundness - roughness / 2, h=z, $fn=resolution);
        }
    }
}

// Connection slot "Dovetail"
module dovetail_slot(x, y1, y2, z, roundness, resolution = 20)
{
    union()
    {
        hull()
        {
            translate([z / 2, 0, z / 2])
            cube([z + 1, y1, z], center=true);
            translate([x / 2 - roundness / 2, y2 / 2 - roundness / 2, 0])
            cylinder(d=roundness, h=z, $fn=resolution);
            translate([x / 2 - roundness / 2, -y2 / 2 + roundness / 2, 0])
            cylinder(d=roundness, h=z, $fn=resolution);
        }
        mirror([1, 0, 0])
        hull()
        {
            translate([z / 2, 0, z / 2])
            cube([z + 1, y1, z], center=true);
            translate([x / 2 - roundness / 2, y2 / 2 - roundness / 2, 0])
            cylinder(d=roundness, h=z, $fn=resolution);
            translate([x / 2 - roundness / 2, -y2 / 2 + roundness / 2, 0])
            cylinder(d=roundness, h=z, $fn=resolution);
        }
    }
}


// Connection hook for slot wall
module slothook(x, y, z, toolsize, roundess, resolution=20)
{
    difference()
    {
        hull()
        {
            translate([-x / 2 + roundness / 2, -y + z + roundness / 2, z / 2])
            cylinder(d=roundness, h=z-roughness, center=true, $fn=resolution);
            translate([x / 2 - roundness / 2, -y + z + roundness / 2, z / 2])
            cylinder(d=roundness, h=z-roughness, center=true, $fn=resolution);
            translate([0, z / 2, z / 2])
            cube([x, z, z], center=true);
        }
        translate([x / 2.5, -z/2, 0])
        dogbone_slot(x, z, z, toolsize, roughness);
    }
}


// Placeholder for "DogBone", "TBone", and "Sniglet". 
// This is used to remove the needed space for plug modules, 
// which will then merge with part via union 
module placeholder(x, y, z, tool_size)
{
    translate([0, y / 2, z / 2])
    cube([x + (toolsize * 4), y, z], center = true);
}


// Uncomment module example and press F5/F6 to render
//
//tbone_plug(x, y, z, toolsize, roughness, edged = -1);
//tbone_slot(x, y, z, toolsize, roughness, edged = 0);
//dogbone_plug(x, y, z, toolsize, roughness, edged = -1);
//dogbone_slot(x, y, z, toolsize, roughness, edged = 0);
//sniglet_slot(x, y, z, toolsize, roughness, edged = -1);
//twedge_plug(40, y, z, toolsize, roughness, cap = 1, resolution = 40);
//twedge_slot(40 , y, thickness, toolsize, roughness, resolution = 40);
//dovetail_plug(x * 2, y1, y2, thickness, roundness, roughness, resolution = 20);
//dovetail_slot(x * 2, y1, y2, thickness, roundness, resolution = 20);
//slothook(thickness * 3 - roughness, thickness * 3 - roughness, thickness-roughness, toolsize, roundness, 40);
//placeholder(x, y, z, toolsize);


// CONNECTORS FOR 3D PRINT DESIGNS -----------------------------------------------------------------------------------------------

