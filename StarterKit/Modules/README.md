
## Starterkit >> Modules

Modules are like libraries in other programming languages, that can be import easy in your designs.

<br>

### How to use module library files?

1. Copy module file to the right place

   - Just copy module files into /files of your project (using the template of the Starterkit)
   - OpenSCAD has a library folder, which can be accessed via File >> Show Library Folder, and copy the files in this folder (Recommended)


2. Import the module files into the design file by adding following lines (for both methods above):


        >include<lot_connectors.scad>
        >
        >include<lot_utilities.scad>

3. Now the modules are ready to use.

        dogbone_slot(90, 20, 18, 6, 0.1)

      or

        dogbone_slot(fillet_x, fillet_y, fillet_z, tool_size, roughness)


 #### ***!To keep the design parametric, you will need to use parameters with values (2nd example)!***
<br>

### Content of module libraries

<br>

#### lot_connectors.scad >> Connector modules for CNC Cutout and 3D Print Designs


| Module | Image |
|----------|----------|
| dogbone_plug(x, y, z, toolsize, roughness, resolution = 20) | <img src="images/connector_dogbone_plug.png" width="256"/> |
| dogbone_slot(x, y, z, toolsize, roughness, edged = 0, resolution = 20) |<img src="images/connector_dogbone_slot.png" width="256"/>|
| dogbone_plug(x, y, z, toolsize, roughness, edged = 1, resolution = 20)<br> <br> Note: edged = 1 for left side, edged = -1 for right side. Default: edged = 0|<img src="images/connector_dogbone_plug_edged.png" width="256"/>|
| dogbone_slot(x, y, z, toolsize, roughness, edged = 1, resolution = 20)<br> <br> Note: edged = 1 for left side, edged = -1 for right side. Default: edged = 0|<img src="images/connector_dogbone_slot_edged.png" width="256"/>|
| tbone_plug(x, y, z, toolsize, roughness, resolution = 20) | <img src="images/connector_tbone_plug.png" width="256"/> |
| tbone_slot(x, y, z, toolsize, roughness, resolution = 20) | <img src="images/connector_tbone_slot.png" width="256"/> |
| tbone_plug(x, y, z, toolsize, roughness, edged = 1, resolution = 20)<br> <br> Note: edged = 1 for left side, edged = -1 for right side. Default: edged = 0|<img src="images/connector_tbone_plug_edged.png" width="256"/>|
| tbone_slot(x, y, z, toolsize, roughness, edged = 1, resolution = 20)<br> <br> Note: edged = 1 for left side, edged = -1 for right side. Default: edged = 0|<img src="images/connector_tbone_slot_edged.png" width="256"/>|
| sniglet_slot(x, y, z, toolsize, roughness, resolution = 20) | <img src="images/connector_sniglet_slot.png" width="256"/> |
| sniglet_slot(x, y, z, toolsize, roughness, edged = 1, resolution = 20)<br> <br> Note: edged = 1 for left side, edged = -1 for right side. Default: edged = 0|<img src="images/connector_sniglet_slot_edged.png" width="256"/>|
| twedge_plug(x, y, z, toolsize, roughness, cap = 0, resolution = 20)|<img src="images/connector_twedge_plug.png" width="256"/>|
| twedge_plug(x, y, z, toolsize, roughness, cap = 1, resolution = 20)|<img src="images/connector_twedge_plug_wCap.png" width="256"/>|
| twedge_slot(x, y, z, tool_size, roughness, resolution = 20)|<img src="images/connector_twedge_slot.png" width="256"/>|
| dovetail_plug(x, y1, y2, z, roundness, roughness, resolution = 20)|<img src="images/connector_dovetail_plug.png" width="256"/>|
| module dovetail_slot(x, y1, y2, z, roundness, resolution = 20)|<img src="images/connector_dovetail_slot.png" width="256"/>|
| module slothook(x, y, z, toolsize, roundess, resolution=20)|<img src="images/connector_slothook.png" width="256"/>|
| placeholder(x, y, z, toolsize) | <img src="images/connector_placeholder.png" width="256"/> |

<br>

#### lot_utils.scad >> Support modules that facilitates 3D modeling in OpenSCAD

| Module | Image |
|----------|-----------|
| triangular_prism(p1, p2, p3, height, scale) | <img src="images/utility_triangular_prism.png" width="256"/> |
| round_edged_cylinder(height, radius, edge_radius, resolution = 0) | <img src="images/utility_round_edged_cylinder_wo_fn.png" width="256"/> |
| round_edged_cylinder(height, radius, edge_radius, 20) | <img src="images/utility_round_edged_cylinder_w_fn.png" width="256"/> |
| linear_array(count, distance) | <img src="images/utility_linear_array.png" width="256"/> |
| linear_array_x(count_x, distance_x) | <img src="images/utility_linear_array_x.png" width="256"/> |
| linear_array_y(count_y, distance_y) | <img src="images/utility_linear_array_y.png" width="256"/> |
| linear_array_z(count_z, distance_z) | <img src="images/utility_linear_array_z.png" width="256"/> |
| polar_array_x(count_x, radius) | <img src="images/utility_polar_array_x.png" width="256"/> |
| polar_array_y(count_y, radius) | <img src="images/utility_polar_array_y.png" width="256"/> |
| polar_array_z(count_z, radius) | <img src="images/utility_polar_array_z.png" width="256"/> |
| flat(f = true) <br> (Design need to be embedded in union())| <img src="images/utility_flat.png" width="256"/> |
