## Collection/Design (e.g. Basic solids) >> FileName without .scad (e.g. Sphere)

### Title
Name of the Design
If it is part of a collection, please set the collection name, style, or topic name first.
(e.g. BBCool >> Bar stool)
<br>

### Author
Name of Designer
In case of modified versions the name of the modifier, as names of original designer will remain in code
<br>

### Date
Date of upload
<br>

### Images
Table of image, with thumbnail (even linked thumbnails to bigger sized images)
Size is 256 x 256
<br>

| Assembled    | CNC Version     |
| :------------- | ---------- |
| <img src="images/ph_lot.jpg" width="256"/> |<img src="images/ph_lot.jpg" width="256"/>|
| Final IRL 1  | Final IRL 2     |
| <img src="images/Template.jpg" width="256"/> | <img src="images/ph_lot.jpg" width="256"/> |

<br>

### 3d Preview
The design can be viewed in 3D by clicking on the file with .stl extension under /files
<br>

### Description
About the design, its design group (e.g. Topic, Style, Type), its use and comments.
<br>


### BOM
Table of Parts, including external parts, that has been used unchanged, or with specific values (e.g. height, width, depth)
<br>

### Notes
Specific notes for production, usage, materials, Machine settings s.o.
<br>

### Modification note
If this design is a modified version, here is the place a general description about changes, that have been made.
<br>

### Link to original
If modified, enter the link to the original version. Please make sure, this is part of the LoT, or otherwise licensed under the same cc-by-sa 4.0 License
<br>
