/*File Info--------------------------------------------------------------------
File Name:  Template.scad
License:    Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name:   Your name
Date:   dd/mm/yyyy
Desc:   Describe your design + Add name of style, series/collection as well
Usage:  Add information about the use of the design, how to assemble or specific
        information about the making
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any further modification below
//--------------------------------------------------------------------------------

//If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below or add your personal modules to it
//include<lot_connectors.scad>
//include<lot_utilities.scad>


//Name of Design

//Parametric values
diameter=20;
fn=80;

//Name the part of the design
//Please help with commenting the code, so other people and you yourself can follow the code easier
//Create a sphere with parametric values diameter
sphere(diameter, $fn=fn);
