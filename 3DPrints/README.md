![](3DPrints.png)
## 3D Prints
</br>
3D Prints contains designs, that can be printed with 3D printers. It can be stand-alone designs or part of other designs (e.g. furniture, sheds)

### Subcategories of 3DPrint:
</br>

* **Houses&Sheds** >> Contains elements or parts needed to build houses, or house-like structures
* **Interior** >> Includes furniture parts, and stand-alone household items
* **Other** >> Are all designs, that do not match with the other 3 Subcategories, including toys, tools, fashion and other fun stuff
* **Outdoors** >> Things you need in the garden, on the field, outside of the house, or anywhere else.
