![](HowTo.png)
## HowTo

The HowTo section will give you tutorials, guidelines and help that you might need to design objects with OpenSCAD, realized those designs physically with machines, and how to use our Library of Things, including template use and git upload.

### Subcategories

* **CAD** >> Is short for Computer-aided Design and contains tutorials and links to help with OpenSCAD
* **CAM** >> or Computer-aided Manufacturing will show you how to prepare designs for CNC cutting or 3D Printing, including settings, tips and tricks learn through experience
* **LoT** >> Mainly tells you about the idea behind the structure of the Library of Things. *How do I find things? How do I upload and modify designs using the template? What is about the naming convention and descriptions in the scad files?*
