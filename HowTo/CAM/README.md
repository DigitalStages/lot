![](HowTo_CAM.png)
## HowTo >> CAM


### Table of Contents ###

#### [HowTo: CNC Router](#cncmachines) ####

* [DXF>>GCODE](#dxf>>gcode)




## HowTo: CNC Router

CNC Machines gives information and guidance about preparing designs and using CNC Routers

### DXF>>GCODE

#### How to export a dxf file from OpenSCAD and prepare it for a CNC Router its needed format (usually .gcode or .ngc)?
<br>

1. First of all, you need to use projection() in OpenSCAD to flatten your design to a cutout. Then Press F6.

![](images/Cutout_Guide_1.png)

2. Go to File >> Export DXF, give it a name and export it.
3. Next you need to prepare the cutout paths in a software.

   * In my journey to find a usable open-source CNC software for Windows, Linux and Mac, to closest of usable, although very buggy, was [PyCam](https://pycam.sourceforge.net)
   * However, I found a affordable brilliant software. As LoT mostly uses designs, that cuts out a design >> [Cut2D](https://www.vectric.com/products/cut2d-desktop) is momentarily the most usable for this matter with a good price of €135 (excludes local taxes).
   * The following steps are done with this software. There is a trial version of it, so you can install this and proceed with it.

<br>

4. Open Cut2D Desktop >> Go to Menu >> File >> New

![](images/Cutout_Guide_2.png)

5. On the Left, a panel is appearing. Here you need to set following settings:
   * Job Type: SingleSided
   * Units: mm
   * Job Size: The size of your board, that is going to be used
   * Z Zero Position: Material Surface (Most CNC Routers)
   * XY Datum Position: X>>0 and Y>>0 (Most CNC Routers)

<br>

![](images/Cutout_Guide_3.png)

6. Time to import your .dxf file. Go to Menu >> File >> Import Vectors >> Choose your .dxf File (If there comes an Warning Message...just press OK)

![](images/Cutout_Guide_4.png)

7. With "Edit Objects" and "Transform Objects", you can select parts of your imported Design, and move it, rotate it, mirror it s.o. in case it does not match your board size.

> Keep in mind that your board will be attached to another board on the CNC Machine by screws or brackets. Leave some space for it!

![](images/Cutout_Guide_5.png)

![](images/Cutout_Guide_6.png)

8. After you positioned your elements, we are going to create the cut paths. For this, go to the right side of the main window and press "Toolpaths". Check the little pin symbol to prevent the auto-hide function of the panel to get re-enacted.

![](images/Cutout_Guide_7.png)

9. First, select all parts, that will be cut inwards. Meaning slots, holes s.o. Use SHIFT + LMB, or drag a box with LMB to group select all inline elements.
10. The symbol for cutouts is under "Toolpath Operations" on the top left "Profile Toolpath". Click it! This will change the panel view.

![](images/Cutout_Guide_8.png)

11. First change the Cut Depth to the thickness of your board

![](images/Cutout_Guide_10.png)

12. Next, select the cutter you are going to use. There is already a database of cutters available, including the different speeds and feed rates depend on cut material and cutters flutes. So check it out, you might find the one you need already there. If not (like in my case), got to "Edit..." and create your own.

  > There are free calculators online to help you creating your own cutter settings, e.g. https://www.cnccookbook.com/cnc-feed-rate-calculator/

13. After, you selected your cutter, you can also change how much the cutter cuts away in each layer. Sometimes, it is better to start with a smaller first layer, and get more in the following layers. This can be changed in settings too.

![](images/Cutout_Guide_9.png)

14. In order to make it a path, that cuts out inwards >> Change Machine Vectors to Inside/Left

![](images/Cutout_Guide_11.png)

15. To prevent the pieces of falling of your jam the router, the cut out pieces need to stay in place by using tabs, or little bridges. Check "Add tabs to toolpath"
16. First, you can keep this default values, but you might want to change it depend on element size and material destiny. You need to cut the bridges later, so the smaller they are, the better. Too small bridges break away tho.

![](images/Cutout_Guide_12.png)

17. Click Edit Tabs and distribute tabs in a "As little as possible, as much as needed" manner using your LMB. Hold LMB pressed to move tabs around.

![](images/Cutout_Guide_13.png)

18. Almost done with the first round. Give the path a name, and press "Calculate"

![](images/Cutout_Guide_14.png)

19. The view changes to "3D View" to show you the result. Go straight back by clicking "2D View" on top left of the Viewer.

![](images/Cutout_Guide_15.png)

20. Now select all elements / edges you want to cut along the outside, and press the same "Toolpath Operation" symbol in the right panel.

![](images/Cutout_Guide_16.png)

21. Keep all settings in the first section, but change "Machine Vectors..." to Outside / Right this time

![](images/Cutout_Guide_17.png)

22. Again, you need to apply tabs to the outside path. Therefore click first "Add tabs to toolpath", then "Edit Tabs ..."

![](images/Cutout_Guide_18.png)

23. Use the LMB to contribute tabs to your outlines. After your finished, press "Close"

![](images/Cutout_Guide_19.png)

24. Give your path a name and press "Calculate"

![](images/Cutout_Guide_20.png)

25. After it is calculated, and the 3D view shows up, go to the right side panel and press "Preview All Toolpaths". This shows you an simulation, how the CNC Router will cut the elements. Press "Close"

![](images/Cutout_Guide_21.png)

26. Now select the checkbox of Toolpaths, so all paths are selected

![](images/Cutout_Guide_22.png)

27. Press the "Save Toolpath" in "Toolpath Operations"

![](images/Cutout_Guide_23.png)

28. Select "Visible toolpaths to one file", as we do not want to change the cutter or anything of that kind. All at once is fine.
29. Next check your CNC Router machine / Controller Software which data format it likes most. In my case, it is EMC2 Arcs(mm)(*.ngc). If you are not sure, I suggest checking on all formats with .ngc extension. Most CNC Routers work with this format.

![](images/Cutout_Guide_24.png)

30. Name your gcode file, and take it to your CNC Router.

![](images/Cutout_Guide_25.png)

Good luck and have fun!
> Please add and change this guide, if there are any important additions / changes to make. Many thanks!
