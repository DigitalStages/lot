/*File Info--------------------------------------------------------------------
File Name:  ShelfMount_v1_Slotwall.scad
License:    Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name:   Jens Meisner
Date:   08/08/2021
Desc:   Shelf mount for wardrobe / book shelf as part of a wall slot system
Usage:  A slot board or wall is necessary to use this mount

/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any further modification below
//--------------------------------------------------------------------------------

//If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below or add your personal modules to it
include<lot_connectors.scad>
include<lot_utilities.scad>


//WardrobeHook for slot wall

//Parametric values
roundness = 10;
resolution = 80;
roughness = 0.001;
thickness = 10;
toolsize = 2;
x = thickness * 3;
y = thickness * 3;
z = thickness;
mount_x = 150;
mount_y = 250;
mount_edge = 30;
board_x = z;
board_y = 230;

// Set face count as resolution for cylinder
$fn = resolution;
// If true design gets flatten to export as dxf
dxf = true;



flat(dxf)
union()
{
    // Add center part with connector
    union()
    {
        difference()
        {
            union()
            {
                // Main part of mount
                hull()
                {
                    translate([-mount_edge / 2, 0, z / 2])
                    cylinder(d=mount_edge, h=z, center=true);
                    translate([0, mount_y - mount_edge / 4, z / 2])
                    cylinder(d=mount_edge, h=z, center=true);
                    translate([mount_x / 2 + mount_edge /2 , 0, z / 2])
                    cylinder(d=mount_edge, h=z, center=true);
                }
                // Mount support on wall side
                hull()
                {
                    translate([-mount_edge / 2, 0, z / 2])
                    cylinder(d=mount_edge, h=z, center=true);
                    translate([mount_x - mount_edge*2, 0, z / 2])
                    cylinder(d=mount_edge, h=z, center=true);
                }
            }
                
            // Cut off mount edge 
            translate([mount_edge, -10, z / 2])
            cube([mount_x, 20, z], center=true);
            // Cut out round curve on main part of mount
            translate([mount_y / 3.3 , mount_y + mount_edge / 2.5, z / 2])
            rotate([0, 0, 5])
            scale([1/4, 1, 1])
            cylinder(d=mount_y*2, h=z, center=true, $fn=resolution*2);
            // Cut out hole for shelf board
            translate([0, board_y / 2 + z*1.5, 0])
            dogbone_slot(board_x, board_y, z, toolsize, roughness, resolution=resolution);
            // Add placeholder for connector to free space for dog bone slot holes
            translate([-roundness / 2, 0, 0])
            placeholder(x - toolsize * 4, z, z, toolsize);
            // Add placeholder for connector to free space for dog bone slot holes
            translate([mount_x / 2 -roundness/2, 0, 0])
            placeholder(x - toolsize * 4, z, z, toolsize);
        }
        // Upper slot connector
        translate([-roundness / 2, 0, 0])
        slothook(x, y , z, toolsize, roughness, resolution);
        // Lower slot connector
        translate([mount_x / 2 -roundness/2, 0, 0])
        slothook(x, y , z, toolsize, roughness, resolution);
    }

}