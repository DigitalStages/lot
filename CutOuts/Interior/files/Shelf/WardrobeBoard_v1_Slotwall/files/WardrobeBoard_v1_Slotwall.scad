/*File Info--------------------------------------------------------------------
File Name:  WardrobeHook_v1_Slotwall.scad
License:    Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name:   Jens Meisner
Date:   06/08/2021
Desc:   Board for wardrobe as part of a wall slot system
Usage:  This is one board is necessary to use slotwall hooks and mounts
        Needs space buffers behind the wall according to thickness

/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any further modification below
//--------------------------------------------------------------------------------

//If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below or add your personal modules to it
include<lot_connectors.scad>
include<lot_utilities.scad>


//WardrobeHook for slot wall

//Parametric values
resolution=80;
roughness=0.001;
thickness = 10;
toolsize = 2;
x = thickness * 3;
y = thickness;
z = thickness;
board_x = 320;
board_y = 500;
board_z = thickness;
// Set face count as resolution for cylinder
$fn = resolution;
// If true design gets flatten to export as dxf
dxf = false;



flat(dxf)
// Add center part with connector
union()
{
    difference()
    {
        // Main part board
        translate([0, 0, z / 2])
        cube([board_x, board_y, board_z], center=true);
        union()
        {
            //Slot shelf mount left top
            translate([ -70, -board_y / 2 + board_y / 10, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
            //Slot shelf mount left bottom
            translate([ 5, -board_y / 2 + board_y / 10, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
            //Slot hook mount left 1
            translate([ 25, -board_y / 2 + board_y / 6, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
            //Slot hook mount left 2
            translate([ 25, -board_y / 2 + board_y / 3, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
        }
        mirror([0, -1, 0])
        union()
        {
            //Slot shelf mount right top
            translate([ -70, -board_y / 2 + board_y / 10, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
            //Slot shelf mount right bottom
            translate([ 5, -board_y / 2 + board_y / 10, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
            //Slot hook mount right 1
            translate([ 25, -board_y / 2 + board_y / 6, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
            //Slot hook mount right 2
            translate([ 25, -board_y / 2 + board_y / 3, 0])
            dogbone_slot(x, y, z, toolsize, roughness);
        }
        //Slot hook mount right 2
        translate([ 25, 0, 0])
        dogbone_slot(x, y, z, toolsize, roughness);

    }

}
