## CNC Cutouts >> Interior >> Shelf >> WardrobeHook_v1_Slotwall

### Title
Slotwall >> WardrobeHook_v1
<br>

### Author
Jens Meisner
<br>

### Date
10/08/2021
<br>

### Images
<br>

| Assembled    | CNC Version    |
| :------------- | ---------- |
| <img src="images/WardrobeHook_Slotwall_1.jpg" width="256"/> |<img src="images/WardrobeHook_Slotwall_2.png" width="256"/>|
| Final IRL 1    | Final IRL 2    |
| <img src="images/WardrobeHook_Slotwall_3.jpg" width="256"/> | <img src="images/WardrobeHook_Slotwall_4.jpg" width="256"/> |

<br>

### 3d Preview
The design can be viewed in 3D by clicking on the file with .stl extension under /files
<br>

### Description
This design is part of the overall section called Slotwall. This particular part is a hook for a wardrobe.
<br>


### BOM
Table of Parts, including external parts, that has been used unchanged, or with specific values (e.g. height, width, depth)
<br>

### Notes
Specific notes for production, usage, materials, Machine settings s.o.
<br>

### Modification note
If this design is a modified version, here is the place a general description about changes, that have been made.
<br>

### Link to original
If modified, enter the link to the original version. Please make sure, this is part of the LoT, or otherwise licensed under the same cc-by-sa 4.0 License
<br>
