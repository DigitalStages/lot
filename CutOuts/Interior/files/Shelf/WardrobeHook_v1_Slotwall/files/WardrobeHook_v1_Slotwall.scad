/*File Info--------------------------------------------------------------------
File Name:  WardrobeHook_v1_Slotwall.scad
License:    Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name:   Jens Meisner
Date:   06/08/2021
Desc:   Hook for wardrobe as part of a wall slot system
Usage:  A slot board or wall is necessary to use this hook

/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any further modification below
//--------------------------------------------------------------------------------

//If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below or add your personal modules to it
include<lot_connectors.scad>
include<lot_utilities.scad>


//WardrobeHook for slot wall

//Parametric values
roundness=10;
resolution=80;
roughness=0.001;
thickness = 8;
toolsize = 2;
x = thickness * 3;
y = thickness * 3;
z = thickness;
hook_x = 90;
hook_y = roundness * 3.5;
hook_z = thickness;
hooksize = 12;
// Set face count as resolution for cylinder
$fn = resolution;
// If true design gets flatten to export as dxf
dxf = false;



flat(dxf)
union()
{
    //Add Top hook part
    hull()
    {
        translate([-hook_x/2, hook_y - hooksize / 2, z / 2])
        cylinder(d=hooksize, h=thickness, center=true);
        translate([-hook_x * 1/3, hooksize/2, z / 2])
        cylinder(d=hooksize, h=thickness, center=true);
    }
    // Add center part with connector
    union()
    {
        difference()
        {
            hull()
            {
                translate([-hook_x * 1/3, hooksize/2, z / 2])
                cylinder(d=hooksize, h=thickness, center=true);
                translate([hook_x * 1/3 - roundness / 2, hooksize/2, z / 2])
                cylinder(d=hooksize, h=thickness, center=true);
            }
            // Add placeholder for connector to free space for dog bone slot holes
            placeholder(x - toolsize * 4, z, z, toolsize);
        }
        //Slot hook connector
        slothook(x, y , z, toolsize, roughness, resolution);
    }
    // Add actual hook on bottom
    difference()
    {
        // Add hook shape
        translate([hook_x / 2 - hook_y * 1/2, hook_y * 1/2 ,z / 2])
        cylinder(d=hook_y, h=thickness, center=true);
        // Cutout 
        hull()
        {
            translate([-hook_x * 1/3, hooksize*1.5, z / 2])
            cylinder(d=hooksize, h=thickness, center=true);
            translate([hook_x * 1/3 - roundness / 2, hooksize*1.5, z / 2])
            cylinder(d=hooksize, h=thickness, center=true);
        }
    }
}