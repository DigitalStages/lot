## CutOuts >> Interior >> Table >> DiningTable_Rectangular_v1

### Title
DiningTable_Rectangular_v1
<br>

### Author
Jens Meisner
<br>

### Date
29/07/2021
<br>

### Images
<br>

| Assembled    | CNC Version    |
| :------------- | ---------- |
| <img src="images/DiningTable_Rectangular_v1_1.jpg" width="256"/> |<img src="images/DiningTable_Rectangular_v1_2.jpg" width="256"/>|
| Final IRL      | Image 4    |
| <img src="images/DiningTable_Rectangular_v1_3.jpg" width="256"/> | <img src="images/ph_lot.jpg" width="256"/> |

<br>

### 3d Preview
The design can be viewed in 3D by clicking on the file with .stl extension under /files
<br>

### Description
This table is designed to be in use without screws
<br>


### BOM
Table of Parts, including external parts, that has been used unchanged, or with specific values (e.g. height, width, depth)
<br>

### Notes
Specific notes for production, usage, materials, Machine settings s.o.
<br>

### Modification note
If this design is a modified version, here is the place a general description about changes, that have been made.
<br>

### Link to original
If modified, enter the link to the original version. Please make sure, this is part of the LoT, or otherwise licensed under the same cc-by-sa 4.0 License
<br>
