/*File Info--------------------------------------------------------------------
File Name:  DiningTable_Rectangular_v1.scad
License:    Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name:   Jens Meisner
Date:   23/06/2021
Desc:   Dining table made of 4 pieces
Usage:  Add information about the use of the design, how to assemble or specific
        information about the making
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any further modification below
//--------------------------------------------------------------------------------

//If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below or add your personal modules to it
include<lot_connectors.scad>
include<lot_utilities.scad>


//Rectangular Dining Table v1

//Parametric values
// Parameters for overall design
tool_size = 2;
thickness = 8;
length = 200;
width = 130;
height = 100;
leg_distance = length - length * 0.2;
leg_size = 20;
bridge_height = leg_size;
resolution = 100;
assembled = 1;   // [0:No, 1:Yes]
dxf = true; // This is necessary to export flat cutouts in dxf format

// Parameters for connectors
fillet_x = thickness * 4;
fillet_y = thickness;
fillet_z = thickness;
roughness = 0.01;

// Display of Desktop CNC Machine range or board size, if flat is active
board_x = 220;
board_y = 320;
board_z = 8;
if(assembled == 0)
{
    %cube([board_x, board_y, board_z]);
}

//Name the part of the design
//Please help with commenting the code, so other people and you yourself can follow the code easier
//Create module for Leg parts
module tabletop(fillet_x, fillet_y, fillet_z, length, width, thickness, tool_size)
{
    difference()
    {
        //Basis board element
        translate([0, 0, thickness / 2])
        cube([length, width, thickness], center=true);
        //Cut out connectors
        union()
        {
            translate([leg_distance / 2 - thickness, width / 2, 0])
            rotate([0, 0, 90])
            tbone_slot(leg_size * 3, fillet_y, thickness, tool_size, roughness, resolution);
            translate([-leg_distance / 2 + thickness, width / 2, 0])
            rotate([0, 0, 90])
            tbone_slot(leg_size * 3, fillet_y, thickness, tool_size, roughness, resolution);
            mirror([0, -1, 0])
            {
                translate([leg_distance / 2 - thickness, width / 2, 0])
                rotate([0, 0, 90])
                tbone_slot(leg_size * 3, fillet_y, thickness, tool_size, roughness);
                translate([-leg_distance / 2 + thickness, width / 2, 0])
                rotate([0, 0, 90])
                tbone_slot(leg_size * 3, fillet_y, thickness, tool_size, roughness);
            }
        }
    }
}


//Bridge that connects both leg parts and get locked with table top
module leg_bridge(fillet_x, fillet_y, fillet_z, leg_distance, bridge_height, thickness, tool_size)
{
    difference()
    {
        translate([0, 0, thickness / 2])
        cube([leg_distance + leg_distance * 0.05, bridge_height, thickness], center=true);
        union()
        {
            translate([leg_distance / 2 - thickness, bridge_height , 0])
            rotate([0, 0, 90])
            tbone_slot(bridge_height * 2, fillet_y, thickness, tool_size, roughness);
            mirror([-1, 0, 0])
            {
                translate([leg_distance / 2 - thickness, bridge_height , 0])
                rotate([0, 0, 90])
                tbone_slot(bridge_height * 2, fillet_y, thickness, tool_size, roughness);
            }
        }
    }
}



//Module of leg
module leg(fillet_x, fillet_y, fillet_z, height, width, leg_size, thickness, tool_size)
{
    difference()
    {
        difference()
        {
            //Create base element of leg
            translate([0, 0, thickness / 2])
            cube([width, height - thickness, thickness], center=true);
            translate([0, bridge_height / 2, thickness / 2])
            cube([width - leg_size * 2, height - thickness - bridge_height, thickness], center=true);
        }
        union()
        {
            //Cutout middle slot for leg bridge
            translate([0, -height / 2 + thickness / 2, 0])
            rotate([0, 0, 90])
            tbone_slot(bridge_height, fillet_y, thickness, tool_size, edged=0, roughness);
            //Cut out placeholder for connector plugs
            translate([-width/2 + leg_size * 0.75 , -height / 2 + thickness / 2, 0])
            placeholder(leg_size * 1.5, fillet_y, fillet_z, tool_size); 
            mirror([1, 0, 0])
            translate([-width / 2 + leg_size * 0.75, -height / 2 + thickness / 2, 0])
            placeholder(leg_size * 1.5, fillet_y, fillet_z, tool_size); 
        }
    }
    //Add connector plugs
    translate([-width / 2 + leg_size * 0.75, -height / 2 + thickness / 2, 0])
    tbone_plug(leg_size * 1.5, fillet_y, thickness, tool_size, edged=-1, roughness);  
    mirror([1, 0, 0])
    translate([-width / 2 + leg_size * 0.75, -height / 2 + thickness / 2, 0])
    tbone_plug(leg_size * 1.5, fillet_y, thickness, tool_size, edged=-1, roughness);  
}


//Position design flat for CNC 
if(assembled == 0)
{
    flat(dxf)
    union()
    {
        translate([length * 0.55, width * 0.61, 0])
        tabletop(fillet_x, fillet_y, fillet_z, length, width, thickness, tool_size);
        translate([length * 0.55, bridge_height * 8.1, 0])
        leg_bridge(fillet_x, fillet_y, fillet_z, leg_distance, bridge_height, thickness, tool_size);
        translate([length * 0.45, height * 2.3, thickness / 2])
        leg(fillet_x, fillet_y, fillet_z, height, width, leg_size, thickness, tool_size);
        translate([length * 0.65, height * 2.55, thickness / 2])
        rotate([0, 0, 180])
        leg(fillet_x, fillet_y, fillet_z, height, width, leg_size, thickness, tool_size);
    }
}
else
{
    union()
    {
        translate([0, 0, height -  thickness])
        tabletop(fillet_x, fillet_y, fillet_z, length, width, thickness, tool_size);
        translate([0, -thickness / 2, height - thickness - bridge_height / 2])
        rotate([-90, 0, 0])
        leg_bridge(fillet_x, fillet_y, fillet_z, leg_distance, bridge_height, thickness, tool_size);
        translate([-leg_distance / 2 + thickness * 1.5, 0, height / 2 - thickness / 2])
        rotate([-90, 0, 90])
        leg(fillet_x, fillet_y, fillet_z, height, width, leg_size, thickness, tool_size);
        translate([leg_distance / 2 - thickness / 2, 0, height / 2 - thickness / 2])
        rotate([-90, 0, 90])
        leg(fillet_x, fillet_y, fillet_z, height, width, leg_size, thickness, tool_size);
    }
}
