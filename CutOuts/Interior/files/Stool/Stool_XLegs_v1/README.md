## CutOuts >> Interior >> Stool >> Stool_XLegs_v1

### Title
Crossleg Stools >> XLegs Version 1
<br>

### Author
Jens Meisner
<br>

### Date
06/06/2021
<br>

### Images

| Assembled    | CNC Version    |
| :------------- | ---------- |
| <img src="images/Stool_XLegs_v1_1.png" width="256"/> |<img src="images/Stool_XLegs_v1_2.png" width="256"/>|
| Final IRL      | Image 4    |
| <img src="images/Stool_XLegs_v1_3.png" width="256"/> | <img src="images/ph_lot.jpg" width="256"/> |
<br>

### 3d Preview
The design can be viewed in 3D by clicking on the file with .stl extension under /files
<br>

### Description
This is one of the design for stools and tables, which needs no screws by using crossed parts connection with slits
<br>


### BOM
No external parts needed
<br>

### Notes
Use roughness value in the design to dedicate how tight the parts fit. Use a lower roughness to make it tighter.
<br>

### Modification note
No modifications yet
<br>

### Link to original
If modified, enter the link to the original version. Please make sure, this is part of the LoT, or otherwise licensed under the same cc-by-sa 4.0 License
<br>
