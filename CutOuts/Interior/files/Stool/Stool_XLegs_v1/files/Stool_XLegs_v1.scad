/* File Info--------------------------------------------------------------------
File Name:  Stool_XLegs_v1.scad
License:    Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name:   Digital Stages
Date:   31/05/2021
Desc:   Design for stools and tables with crossed leg 
Usage:  -
/*
/* Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
// Please continue with any further modification below
//--------------------------------------------------------------------------------

// If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below or add your personal modules to it
// Or just copy lot_... modules into your libraries folder of your OpenSCAD installation
include <lot_connectors.scad>
include <lot_utilities.scad>

// Parameters for overall design
tool_size = 6;
thickness = 18;
seat_size = 320;
leg_height = 430;
foot_size = 80;
resolution = 100;
assembled = 1;   // [0:No, 1:Yes]
dxf = true; // This is necessary to export flat cutouts in dxf format

// Parameters for connectors
fillet_x = thickness * 4;
fillet_y = thickness;
fillet_z = thickness;
roughness = 0.03;

// Display of Desktop CNC Machine range or board size, if flat is active
board_x = 220;
board_y = 320;
board_z = 8;
if(assembled == 0)
{
    %cube([board_x, board_y, board_z]);
}

// Module of Part 1: Seat
module seat(fillet_x, fillet_y, fillet_z, seat_size, thickness, tool_size)
{
    difference()
    {
        translate([0, 0, thickness * 0.5])
        cylinder(h = thickness, d = seat_size, center = true, $fn = resolution);
        polar_array_z(4, seat_size * 0.25) tbone_slot(fillet_x, fillet_y, fillet_z, tool_size, roughness);
    }
}

// Module of Part 2: Leg with slit on top
module leg_btm(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size)
{
    difference()
    {
        union()
        {
            hull()
            {
                translate([-seat_size * 0.4, leg_height-foot_size * 0.5, thickness * 0.5])
                cylinder(h = thickness, d = foot_size, center = true, $fn = resolution);
                translate([-seat_size * 0.25, leg_height  * 0.6, thickness * 0.5])
                cube([seat_size * 0.1, foot_size, thickness], center = true);
            }
            hull()
            {
                translate([seat_size * 0.4, leg_height-foot_size * 0.5, thickness * 0.5])
                cylinder(h = thickness, d = foot_size, center = true, $fn = resolution);            
                translate([seat_size * 0.25, leg_height * 0.6, thickness * 0.5])
                cube([seat_size * 0.1, foot_size, thickness], center = true);
            }
            difference()
            {
                union()
                {
                    hull()
                    {
                        translate([-seat_size * 0.25, thickness, thickness * 0.5])
                        cube([fillet_x + tool_size * 4, foot_size * 0.3, thickness], center = true);
                        translate([-seat_size * 0.15, leg_height * 0.4, thickness * 0.5])
                        cube([seat_size * 0.2, foot_size * 0.5, thickness], center = true);
                    }
                    hull()
                    {
                        translate([seat_size * 0.25, thickness, thickness * 0.5])
                        cube([fillet_x + tool_size * 4, foot_size * 0.3, thickness], center = true);                  
                        translate([seat_size * 0.15, leg_height * 0.4, thickness * 0.5])
                        cube([seat_size * 0.2, foot_size * 0.5, thickness], center = true);
                    }
                    translate([0, leg_height * 0.5, thickness/2])
                    cube([seat_size * 0.6, leg_height * 0.6, thickness], center = true);
                    }
                    translate([-seat_size * 0.25, 0, 0])
                    placeholder(fillet_x, fillet_y, fillet_z, tool_size);
                    translate([seat_size * 0.25, 0, 0])
                    placeholder(fillet_x, fillet_y, fillet_z, tool_size);  
                }
                translate([-seat_size * 0.25, 0, 0])
                tbone_plug(fillet_x, fillet_y, fillet_z, tool_size, roughness);
                translate([seat_size * 0.25, 0, 0])
                tbone_plug(fillet_x, fillet_y, fillet_z, tool_size, roughness);  
        }
        rotate([0, 0, 90])
        tbone_slot(leg_height, fillet_y, fillet_z, tool_size, roughness);
    }
}
 
// Module of Part 3: Leg with slit on bottom
module leg_top(fillet_x,fillet_y,fillet_z,seat_size,leg_height,thickness,tool_size)
{
    difference()
    {
        union()
        {
            hull()
            {
                translate([-seat_size * 0.4, leg_height-foot_size * 0.5, thickness * 0.5])
                cylinder(h = thickness, d = foot_size, center = true, $fn = resolution);
                translate([-seat_size * 0.25, leg_height  * 0.6, thickness * 0.5])
                cube([seat_size * 0.1, foot_size, thickness], center = true);
            }
            hull()
            {
                translate([seat_size * 0.4, leg_height-foot_size * 0.5, thickness * 0.5])
                cylinder(h = thickness, d = foot_size, center = true, $fn = resolution);            
                translate([seat_size * 0.25, leg_height * 0.6, thickness * 0.5])
                cube([seat_size * 0.1, foot_size, thickness], center = true);
            }
            difference()
            {
                union()
                {
                    hull()
                    {
                        translate([-seat_size * 0.25, thickness, thickness * 0.5])
                        cube([fillet_x + tool_size * 4, foot_size * 0.3, thickness], center = true);
                        translate([-seat_size * 0.15, leg_height * 0.4, thickness * 0.5])
                        cube([seat_size * 0.2, foot_size * 0.5, thickness], center = true);
                    }
                    hull()
                    {
                        translate([seat_size * 0.25, thickness, thickness * 0.5])
                        cube([fillet_x + tool_size * 4, foot_size * 0.3, thickness], center = true);                  
                        translate([seat_size * 0.15, leg_height * 0.4, thickness * 0.5])
                        cube([seat_size * 0.2, foot_size * 0.5, thickness], center = true);
                    }
                    translate([0, leg_height * 0.5, thickness/2])
                    cube([seat_size * 0.6, leg_height * 0.6, thickness], center = true);
                    }
                    translate([-seat_size * 0.25, 0, 0])
                    placeholder(fillet_x, fillet_y, fillet_z, tool_size);
                    translate([seat_size * 0.25, 0, 0])
                    placeholder(fillet_x, fillet_y, fillet_z, tool_size);  
                }
                translate([-seat_size * 0.25, 0, 0])
                tbone_plug(fillet_x, fillet_y, fillet_z, tool_size, roughness);
                translate([seat_size * 0.25, 0, 0])
                tbone_plug(fillet_x, fillet_y, fillet_z, tool_size, roughness);  
        }
        translate([0,leg_height,0])
        rotate([0, 0, 90])
        tbone_slot(leg_height, fillet_y, fillet_z, tool_size, roughness);
    }
}

// Display and positioning of stool elements: flat = 1 assembled, flat = 1: in position for CNC machine
if(assembled == 1)
{
    translate([0, 0, leg_height])
    seat(fillet_x, fillet_y, fillet_z, seat_size, thickness, tool_size);
    translate([0, -thickness * 0.5, leg_height])
    rotate([-90, 0, 0])
    leg_top(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size);
    translate([thickness / 2, 0, leg_height])
    rotate([-90, 0, 90])
    leg_btm(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size);
}
else if(assembled == 0)
{
    flat(true)
    union()
    {
        translate([board_x / 1.5, board_y * 0.78, 0])
        seat(fillet_x, fillet_y, fillet_z, seat_size, thickness, tool_size);
        translate([board_x / 3.5, board_y / 4.3, 0])
        rotate([0, 0, 0])
        leg_top(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size);
        translate([board_x * 0.7, board_y * 0.4, 0])
        rotate([0, 0, 180])
        leg_btm(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size); 
    }
}