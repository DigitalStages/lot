/* File Info--------------------------------------------------------------------
File Name:  Stool_XLegs_v2.scad
License:    Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name:   Digital Stages - Jens Meisner
Date:   31/05/2021
Desc:   Design for stools and tables with crossed leg 
Usage:  -
/*
/* Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
// Please continue with any further modification below
//--------------------------------------------------------------------------------

// If you want to use module files, please copy the module .scad files into the files folder and uncomment following lines below or add your personal modules to it
// Or just copy lot_... modules into your libraries folder of your OpenSCAD installation
include<lot_connectors.scad>
include<lot_utilities.scad>

// Parameters for overall design
tool_size = 2;
thickness = 8;
seat_size = 120;
leg_height = 150;
foot_size = 30;
resolution = 100;
assembled = 0; // [0: No, 1: Yes] Shows the furniture in final assembly or a cut layout
dxf = true; // [true: export as dxf, false: export as stl]
 
// Parameters for connectors
fillet_x = thickness*4;
fillet_y = thickness;
fillet_z = thickness;
roughness = 0.03;

// Display of Desktop CNC Machine range or board size, if flat is active
board_x = 220;
board_y = 320;
board_z = 8;
if(assembled == 0)
{
    %cube([board_x, board_y, board_z]);
}

// Module of Part 1: Seat
module seat(fillet_x, fillet_y, fillet_z, seat_size, thickness, tool_size)
{
    difference()
    {
        cylinder(d=seat_size, h=thickness, $fn = resolution);
        dogbone_slot(seat_size * 0.6, fillet_y, fillet_z, tool_size, roughness);
        rotate([0, 0, 90]) 
        dogbone_slot(seat_size * 0.6, fillet_y, fillet_z, tool_size, roughness);
    }
}

// Module of Part 2: Leg with slit on top
module leg_btm(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size)
{
    difference()
    {
        union()
        {
            difference()
            {
                union()
                {
                    hull()
                    {
                        translate([-seat_size / 1.7, leg_height - (foot_size / 2), thickness / 2])
                        scale([0.5, 1, 1])
                        cylinder(d = foot_size, h = thickness, center = true);
                        translate([seat_size / 1.7, leg_height - ( foot_size / 2), thickness / 2])
                        cylinder(d = foot_size, h = thickness, center = true);
                    }
                    hull()
                    {
                        translate([seat_size / 1.7, leg_height-(foot_size / 2), thickness / 2])
                        cylinder(d = foot_size, h = thickness, center = true);
                        translate([seat_size / 3.3, thickness * 2, thickness / 2])
                        cube([foot_size, foot_size / 2, thickness], center = true);
                    }
                    hull()
                    {
                        translate([seat_size / 3.3, foot_size / 2, thickness / 2])
                        cube([foot_size + foot_size * 0.1, foot_size, thickness], center = true);
                        translate([-seat_size / 3.3, foot_size / 2, thickness / 2])
                        cube([foot_size + foot_size * 0.1, foot_size, thickness], center = true);
                    }
                }
                placeholder(seat_size * 0.6, fillet_y, fillet_z, tool_size);
            }
            tbone_plug(seat_size * 0.6, fillet_y, fillet_z, tool_size, roughness);
        }
        translate([0, leg_height-foot_size, 0])
        rotate([0, 0, 90])
        dogbone_slot(foot_size, fillet_y, fillet_z, tool_size, roughness);
        translate([0, -foot_size + foot_size, 0])
        rotate([0, 0, 90])
        dogbone_slot(foot_size, fillet_y, fillet_z, tool_size, roughness);
    }
}

// Module of Part 3: Leg with slit on bottom
module leg_top(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size)
{
    difference()
    {
        union()
        {
            difference()
            {
                union()
                {
                    hull()
                    {
                        translate([-seat_size / 1.7, leg_height - (foot_size / 2), thickness / 2])
                        scale([0.5, 1, 1])
                        cylinder(d = foot_size, h = thickness, center = true);
                        translate([seat_size / 1.7, leg_height - ( foot_size / 2), thickness / 2])
                        cylinder(d = foot_size, h = thickness, center = true);
                    }
                    hull()
                    {
                        translate([seat_size / 1.7, leg_height-(foot_size / 2), thickness / 2])
                        cylinder(d = foot_size, h = thickness, center = true);
                        translate([seat_size / 3.3, thickness * 2, thickness / 2])
                        cube([foot_size, foot_size / 2, thickness], center = true);
                    }
                    hull()
                    {
                        translate([seat_size / 3.3, foot_size / 2, thickness / 2])
                        cube([foot_size + foot_size * 0.1, foot_size, thickness], center = true);
                        translate([-seat_size / 3.3, foot_size / 2, thickness / 2])
                        cube([foot_size + foot_size * 0.1, foot_size, thickness], center = true);
                    }
                }
                placeholder(seat_size * 0.6, fillet_y, fillet_z, tool_size);
            }
            tbone_plug(seat_size * 0.6, fillet_y, fillet_z, tool_size, roughness);
        }
        translate([0, foot_size, 0])
        rotate([0, 0, 90])
        dogbone_slot(foot_size, fillet_y, fillet_z, tool_size, roughness);
        translate([0, leg_height, 0])
        rotate([0, 0, 90])
        dogbone_slot(foot_size, fillet_y, fillet_z, tool_size, roughness);
    }
}


// Display and positioning of stool elements: flat = 1 assembled, flat = 1: in position for CNC machine
if(assembled == 1)
{
    translate([0, 0, leg_height])
    seat(fillet_x, fillet_y, fillet_z, seat_size, thickness, tool_size);
    translate([0, -thickness / 2, leg_height])
    rotate([-90, 0, 0])
    leg_top(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size);
    translate([thickness / 2, 0, leg_height])
    rotate([-90, 0, 90])
    leg_btm(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size);
}
else if(assembled == 0)
{
    flat(dxf)
    union()
    {
        translate([board_x / 1.5, board_y * 0.78, 0])
        seat(fillet_x, fillet_y, fillet_z, seat_size, thickness, tool_size);
        translate([board_x / 1.38, board_y / 2.5, 0])
        rotate([0, 0, 90])
        leg_top(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size);
        translate([board_x * 0.27, board_y * 0.3, 0])
        rotate([0, 0, 270])
        leg_btm(fillet_x, fillet_y, fillet_z, seat_size, leg_height, thickness, tool_size);
    }
}
 
