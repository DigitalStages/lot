![LoT Icon](LoT_icon.png)

## LoT - Library of Things
</br>
LoT is an open-source database of designs created with OpenSCAD, that can be manufactured by CNC Machines, Laser cutter, or 3D Printer.

It is an attempt to create a pool for makers, designers, hobbyists, simply people who would like to participate in multiple manufacturing. Items in and outside of houses, gardens, schools, hobbies, tools for activities, and even parts to build houses.

The parametric designs make it possible to address different materials and sizes of the desired thing. It can be modeled to an extend, that single parts e.g. of furniture can have different parametric values. The advantage of such design is the use of materials with different thicknesses, which will make a production with recycled wood more efficient.

OpenSCAD is a light-weight software. It uses syntax so easy to read, that you even would not need a computer to create blueprints from it. So the designs scripts (.scad) literally can be printed into books, thus creating the Library of Things.

The project includes following main categories:

* **3DPrints** >> Includes all designs created to be printed with a 3D Printer
* **CutOuts** >> Includes designs, that can be produced by a CNC Machine or a Laser cutter
* **HowTo** >> Is the section with guides how to design, how to make, and how to use the template for LoT uploads
* **StarterKit** >> Contains a template to be used for new uploads, modules to simplify the modeling, and examples that show upload ready projects

If you are not familiar with git, you will find information about it in the HowTo >> LoT section.

**PLEASE READ HowTo >> LoT >> Use the template BEFORE UPLOADING ANYTHING!**

The administrator will grant you push requests, if you are interested in becoming a main developer of LoT.

However, if you want your designs (already fit for upload) being added to the library, without any extra effort, you can send a link to the download to contact@jensmeisner.net

*All designs are under cc-by-sa Creative Common License, so we keep the designer in mind, when searching for their styles. We can use the designs for private and commercial use, while keeping it open-source and free for everyone*


>We can build a fair world, even it seems hopeless sometimes. Lets share our ideas and its practical use. Maybe one day we can access a Library of Things in our Public Library as database...or books.
